<?php namespace Decoupled\Core\Output;

use Symfony\Component\Templating\DelegatingEngine;
use ArrayAccess;

class Output extends DelegatingEngine{

    protected $params;

    protected $view;

    public function __toString()
    {
        return $this->output();
    }

    public function __invoke( $view, $params = [] )
    {
        $this->setView( $view );

        if( !empty($params) )
            $this->with( $params );

        return $this;
    }

    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    public function getView()
    {
        return $this->view;
    }

    public function with( $key, $value = null )
    {
        return $this->addParam( $key, $value );
    }

    public function addParam( $key, $value = null )
    {
        if( is_array($key) || $key instanceof ArrayAccess ) 
        {
            foreach( $key as $k => $v )
            {
                $this->addParam( $k, $v );
            }
        }
        else
        {
            $this->params[$key] = $value;
        }

        return $this;
    }

    public function getParam( $key )
    {
        return $this->params[$key];
    }

    public function removeParam( $key )
    {
        unset( $this->params[$key] );

        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function render( $name, array $params = [] )
    {
        $params = array_merge( $this->getParams(), $params );

        return parent::render( $name, $params );
    }

    public function output()
    {
        return $this->render( $this->getView() );
    }


}