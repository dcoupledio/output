<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Application\Application;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Illuminate\Support\Collection;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\PhpEngine;
use Decoupled\Core\Output\TemplateLoader;
use Decoupled\Core\Output\Output;

class OutputTest extends TestCase{

    public function testCanUseTwigEngine()
    {   
        $loader = new Twig_Loader_Filesystem();

        $loader->addPath( dirname(__FILE__), 'app' );

        $twigEnv = new Twig_Environment( $loader );

        $twig = new TwigEngine( $twigEnv, new TemplateNameParser() );

        $output = new Output([ $twig ]);

        $this->assertContains( 
            'king', 
            (string) $output( '@app/file.html.twig', ['title'=>'king'] ) 
        );

        $collection = new Collection(['value' => 'example_value']);

        $output->setView( '@app/file2.html.twig' )->with( $collection );

        $this->assertContains(
            'example_value',
            (string) $output
        );

        return $output;
    }

    /**
     * @depends testCanUseTwigEngine
     */

    public function testCanUsePhpEngine( $output )
    {
        $loader = new TemplateLoader( new Twig_Loader_Filesystem() );

        $loader->addPath( dirname(__FILE__), 'app' );

        $output->addEngine( new PhpEngine( new TemplateNameParser(), $loader ) );

        $this->assertEquals(
            (string) $output('@app/file.html.twig'),
            (string) $output('@app/file.php')
        );

        $this->assertEquals(
            (string) $output,
            $output->output()
        );
    }
}