<?php namespace Decoupled\Core\Output;

use Symfony\Component\Templating\Storage\FileStorage;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\TemplateReferenceInterface;
use Twig_Loader_Filesystem;
use Twig_Error_Loader;

/**
 * TemplateLoader wrappet for Twig_Loader_Filesystem
 * that makes it compatible with 
 * Symfony\Component\Templating\Loader\LoaderInterface
 * implementation
 */

class TemplateLoader extends FilesystemLoader{

    public function __construct( $twig )
    {
        $this->setTwig( $twig );
    }

    public function setTwig( Twig_Loader_Filesystem $twig )
    {
        $this->twig = $twig;

        return $this;
    }

    public function __call( $fn, array $params = array() )
    {
        return call_user_func_array( 
            [$this->twig, $fn], 
            $params 
        );
    }

    public function load(TemplateReferenceInterface $template)
    {
        $file = $this->twig->getCacheKey( $template->get('name'), false );

        return $file ? new FileStorage($file) : false;
    }

}